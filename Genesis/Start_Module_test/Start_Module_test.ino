class StartModule {
	int state = 0, pin = 0;
	int startModulePin = 10;

public:
	StartModule(int _pin = 0) {
		pin = _pin;
		pinMode(pin, INPUT);
	}

	void setPin(int _pin) {
		(*this) = StartModule(_pin);
	}

	bool getState() {
		state = digitalRead(pin);
		return state;
	}

	void init() {
		setPin(startModulePin);
	}

	void printState() {
		Serial.println(getState());
	}
};

class Robot {
public:
	StartModule reciever;
	void init() {
		reciever.init();
	}
};
Robot robot;

void setup() {
	Serial.begin(9600);
	robot.init();
}

void loop() {
	if (robot.reciever.getState()) digitalWrite(13, HIGH);
	else digitalWrite(13, LOW);
}

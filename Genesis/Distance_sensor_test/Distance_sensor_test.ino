const int CNT_DIST_SENSORS = 3;

class DistanceSensor {
	int pin;
	bool state;

public:
	DistanceSensor(int _pin = 0) {
		pin = _pin;
		pinMode(pin, INPUT);
	}

	bool getState() {
		state = !digitalRead(pin);
		return state;
	}

	void setPin(int a) {
		(*this) = DistanceSensor(a);
	}
};


class DistanceSensors {
	int n = CNT_DIST_SENSORS;
	int distanceSensorsPins[CNT_DIST_SENSORS + 1] = {0, A2, A4, A3};
	DistanceSensor ds[CNT_DIST_SENSORS + 1];

public:
	int pos = 0, lastPos = 0;

	DistanceSensors() {}

	void setSensor(int poz, int _pin) {
		ds[poz].setPin(_pin);
	}

	void setSensors(int v[]) {
		for (int i = 1; i <= n; ++ i) setSensor(i, v[i]);
	}
	
	void init() {
		setSensors(distanceSensorsPins);
	}

	void updatePosition() {
		if (pos != 0) lastPos = pos;
		pos = 0;
		for (int i = 1; i <= n; ++ i) {
			if (ds[i].getState())
				pos = i;
		}
	}

	int getPosition() {
		updatePosition();
		return pos;
	}

	int getLastPosition() {
		return lastPos;
	}

	void printPosition(){
		Serial.print(getPosition());
		Serial.print("		");
		Serial.println(getLastPosition());
	}

	void printSensorsValues() {
		for (int i = 1; i <= n; ++ i) {
			Serial.print(ds[i].getState());
			Serial.print("		");
		}
		Serial.println();
	}
};

class Robot {

public:
  DistanceSensors enemy;
	void init() {
		enemy.init();
	}
};
Robot robot;

void setup() {
	Serial.begin(9600);
  	robot.init();
}

void loop() {
  robot.enemy.printPosition();
  delay(100);
}

const int CNT_MOTORS = 2;

class Motor {
	int digitalPin, pwmPin;
	int power = 0, dir = 0;
	bool reverse = 0;
public:
	Motor(int _digitalPin = 0, int _pwmPin = 0, bool _reverse = false) {
		digitalPin = _digitalPin;
		pwmPin = _pwmPin;
		reverse = _reverse;
		pinMode(digitalPin, OUTPUT);
		pinMode(pwmPin, OUTPUT);
		digitalWrite(digitalPin, 0);
		analogWrite(pwmPin, 0);
	}

	void setPins(int _digitalPin, int _pwmPin, bool _reverse) {
		(*this) = Motor(_digitalPin, _pwmPin, _reverse);
	}

	void setPower(int val) {
		if (reverse) val = -val;
		if (val > 0) digitalWrite(digitalPin, 1);
		else digitalWrite(digitalPin, 0);
		val = abs(val);
		val = min(val, 255);
		analogWrite(pwmPin, val);
	}

	void stop() {
		digitalWrite(digitalPin, 0);
		analogWrite(pwmPin, 0);
	}
};

class Motors {
	int n = CNT_MOTORS;
	int power[CNT_MOTORS + 1];
	Motor m[CNT_MOTORS + 1];
	int digitalPins[CNT_MOTORS + 1] = {0, 12, 13};
	int pwmPins[CNT_MOTORS + 1] = {0, 3, 11};
	bool reverse[CNT_MOTORS + 1] = {0, 1, 1};

public:
	Motors() {}

	void setMotor(int poz, int _digitalPin, int _pwmPin, bool _reverse) {
		m[poz].setPins(_digitalPin, _pwmPin, _reverse);
	}

	void setMotors(int v1[], int v2[], bool v3[]) {
		for (int i = 1; i <= n; ++ i) {
			m[i].setPins(v1[i], v2[i], v3[i]);
		}
	}

	void init() {
		setMotors(digitalPins, pwmPins, reverse);
	}

	void setMotorPower(int poz, int val) {
		m[poz].setPower(val);
	}

	void setMotorsPowers(int v[]) {
		for (int i = 1; i <= n; ++ i) m[i].setPower(v[i]);
	}
	
	void motoare(int a, int b) {
		m[1].setPower(a);
		m[2].setPower(b);
	}

	void stop() {
		for (int i = 1; i <= n; ++ i) m[i].stop();
	}
};

class Robot {
public:
  Motors drive;
	void init() {
		drive.init();
	}
};
Robot robot;

void setup() {
	Serial.begin(9600);
  	robot.init();
}

void loop() {
	for (int i = 0; i < 256; ++ i) {
		robot.drive.motoare(i, i);
		delay(10);
	}
	for (int i = 255; i >= 0; -- i) {
		robot.drive.motoare(i, i);
		delay(10);
	}
	for (int i = 0; i < 256; ++ i) {
		robot.drive.motoare(-i, -i);
		delay(10);
	}
	for (int i = 255; i >= 0; -- i) {
		robot.drive.motoare(-i, -i);
		delay(10);
	}
}

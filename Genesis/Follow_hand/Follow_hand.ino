const int CNT_MOTORS = 2;

class Motor {
	int digitalPin, pwmPin;
	int power = 0, dir = 0;
	bool reverse = 0;
public:
	Motor(int _digitalPin = 0, int _pwmPin = 0, bool _reverse = false) {
		digitalPin = _digitalPin;
		pwmPin = _pwmPin;
		reverse = _reverse;
		pinMode(digitalPin, OUTPUT);
		pinMode(pwmPin, OUTPUT);
		digitalWrite(digitalPin, 0);
		analogWrite(pwmPin, 0);
	}

	void setPins(int _digitalPin, int _pwmPin, bool _reverse) {
		(*this) = Motor(_digitalPin, _pwmPin, _reverse);
	}

	void setPower(int val) {
		if (reverse) val = -val;
		if (val > 0) digitalWrite(digitalPin, 1);
		else digitalWrite(digitalPin, 0);
		val = abs(val);
		val = min(val, 255);
		analogWrite(pwmPin, val);
	}

	void stop() {
		digitalWrite(digitalPin, 0);
		analogWrite(pwmPin, 0);
	}
};

class Motors {
	int n = CNT_MOTORS;
	int power[CNT_MOTORS + 1];
	Motor m[CNT_MOTORS + 1];
	int digitalPins[CNT_MOTORS + 1] = {0, 12, 13};
	int pwmPins[CNT_MOTORS + 1] = {0, 3, 11};
	bool reverse[CNT_MOTORS + 1] = {0, 1, 1};

public:
	Motors() {}

	void setMotor(int poz, int _digitalPin, int _pwmPin, bool _reverse) {
		m[poz].setPins(_digitalPin, _pwmPin, _reverse);
	}

	void setMotors(int v1[], int v2[], bool v3[]) {
		for (int i = 1; i <= n; ++ i) {
			m[i].setPins(v1[i], v2[i], v3[i]);
		}
	}

	void init() {
		setMotors(digitalPins, pwmPins, reverse);
	}

	void setMotorPower(int poz, int val) {
		m[poz].setPower(val);
	}

	void setMotorsPowers(int v[]) {
		for (int i = 1; i <= n; ++ i) m[i].setPower(v[i]);
	}
	
	void motoare(int a, int b) {
		m[1].setPower(a);
		m[2].setPower(b);
	}

	void stop() {
		for (int i = 1; i <= n; ++ i) m[i].stop();
	}
};

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

const int CNT_LINE_SENSORS = 2;

class LineSensor {
	int pin, rawVal;
	bool state;
public:
	LineSensor(int _pin = 0) {
		pin = _pin;
		pinMode(pin, INPUT);
	}

	void setPin(int _pin) {
		(*this) = LineSensor(_pin);
	}

	void update() {
		rawVal = analogRead(pin);
		if (rawVal < 400) state = 1;
		else state = 0;
	}

	int getRaw() {
		update();
		return rawVal;
	}

	bool getState() {
		update();
		return state;
	}
};


class LineSensors {
	int n = CNT_LINE_SENSORS;
	int lineSensorsPins[CNT_LINE_SENSORS + 1] = {0, A1, A0};
	LineSensor ls[CNT_LINE_SENSORS + 1];

public:
	int pos = 0;

  	LineSensors() {}

	void setSensor(int poz, int _pin) {
		ls[poz].setPin(_pin);
	}

	void setSensors(int v[]) {
		for (int i = 1; i <= n; ++ i) setSensor(i, v[i]);
	}

	void init() {
		setSensors(lineSensorsPins);
	}

	void updatePosition() {
		pos = 0;
		for (int i = 1; i <= n;  ++ i)
			if (ls[i].getState())
				pos = i;
	}

	int getPosition() {
		updatePosition();
		return pos;
	}

	bool isOnLine() {
		if (getPosition())
			return 1;
		return 0;
	}

	void printPosition() {
		Serial.println(getPosition());
	}

	void printSensorsValues() {
		for (int i = 1; i <= n; ++ i) {
			Serial.print(ls[i].getState());
			Serial.print("		");
		}
		Serial.println();
	}

	void printSensorsRawValues() {
		for (int i = 1; i <= n; ++ i) {
			Serial.print(ls[i].getRaw());
			Serial.print("		");
		}
		Serial.println();
	}

};

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

const int CNT_DIST_SENSORS = 3;

class DistanceSensor {
	int pin;
	bool state;

public:
	DistanceSensor(int _pin = 0) {
		pin = _pin;
		pinMode(pin, INPUT);
	}

	bool getState() {
		state = !digitalRead(pin);
		return state;
	}

	void setPin(int a) {
		(*this) = DistanceSensor(a);
	}
};


class DistanceSensors {
	int n = CNT_DIST_SENSORS;
	int distanceSensorsPins[CNT_DIST_SENSORS + 1] = {0, A2, A4, A3};
	DistanceSensor ds[CNT_DIST_SENSORS + 1];

public:
	int pos = 0, lastPos = 0;

	DistanceSensors() {}

	void setSensor(int poz, int _pin) {
		ds[poz].setPin(_pin);
	}

	void setSensors(int v[]) {
		for (int i = 1; i <= n; ++ i) setSensor(i, v[i]);
	}
	
	void init() {
		setSensors(distanceSensorsPins);
	}

	void updatePosition() {
		if (pos != 0) lastPos = pos;
		pos = 0;
		for (int i = 1; i <= n; ++ i) {
			if (ds[i].getState())
				pos = i;
		}
	}

	int getPosition() {
		updatePosition();
		return pos;
	}

	int getLastPosition() {
		return lastPos;
	}

	void printPosition(){
		Serial.println(getPosition());
	}

	void printSensorsValues() {
		for (int i = 1; i <= n; ++ i) {
			Serial.print(ds[i].getState());
			Serial.print("		");
		}
		Serial.println();
	}
};

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

class StartModule {
	int pin = 0;
	int startModulePin = 10;
  bool state = 0;

public:
	StartModule(int _pin = 0) {
		pin = _pin;
		pinMode(pin, INPUT);
	}

	void setPin(int _pin) {
		(*this) = StartModule(_pin);
	}

	bool getState() {
		state = digitalRead(pin);
		return state;
	}

	void init() {
		setPin(startModulePin);
	}

	void printState() {
		Serial.println(getState());
	}
};

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------

class Robot {
public:
	int speed = 150;

	Motors drive;
	LineSensors line;
	DistanceSensors enemy;
	StartModule reciever;
	void init() {
		drive.init();
		line.init();
		enemy.init();
		reciever.init();
	}
};
Robot robot;

void setup() {
	//Serial.begin(9600);
	robot.init();
	delay(5000);
}

int position;
unsigned long time;

void loop() {
	//robot.drive.stop();
	//while (robot.reciever.getState()) {
		if (!robot.line.isOnLine()) {
			if (robot.enemy.getPosition() != 0) {
				position = robot.enemy.getPosition();
				if (position == 1) robot.drive.motoare(-robot.speed, robot.speed);
				else if (position == 2) robot.drive.motoare(0, 0);
				else if (position == 3) robot.drive.motoare(robot.speed, -robot.speed);
			}
			else {
				position = robot.enemy.getLastPosition();
				if (position == 1) robot.drive.motoare(-robot.speed, robot.speed);
				else if (position == 2 || position == 0) robot.drive.motoare(0, 0);
				else if (position == 3) robot.drive.motoare(robot.speed, -robot.speed);
			}
		}
		else {
			time = millis();
			position = robot.line.pos;
			while (millis() - time <= 300 /*&& robot.reciever.getState()*/) robot.drive.motoare(-robot.speed, -robot.speed);
			time = millis();
			while (robot.enemy.getPosition() == 0 && millis() - time <= 200 /*&& robot.reciever.getState()*/ && !robot.line.isOnLine()) {
				if (position == 1) robot.drive.motoare(robot.speed, -robot.speed);
				else if (position == 2) robot.drive.motoare(-robot.speed, robot.speed);
			}
		}
	//}

}

const int CNT_SWITCHES = 3;

class Switch {
	int pin;
	bool state;

public:
	Switch(int _pin = 0) {
		pin = _pin;
		state = 0;
		digitalWrite(pin, HIGH);
	}

	bool getState() {
		state = digitalRead(pin);
		return state;
	}

	void setPin(int _pin) {
		(*this) = Switch(_pin);
	}
};

class Switches {
	int n = CNT_SWITCHES;
	int switchesPins[CNT_SWITCHES + 1] = {0, 5, 6, 7};
	Switch sw[CNT_SWITCHES + 1];
	int state;

	public:
		Switches() {}

		void setSwitch(int poz, int _pin) {
			sw[poz].setPin(_pin);
		}

		void setSwitches(int v[]) {
			for (int i = 1; i <= n; ++ i) setSwitch(i, v[i]);
		}

		void init() {
			setSwitches(switchesPins);
		}

		void updateState() {
			state = 0;
			for (int i = 1; i <= n; ++ i) {
				if (sw[i].getState())
					state += 1 << (i - 1);
			}
		}

		int getState() {
			updateState();
			return state;
		}

		void printState() {
			Serial.println(getState());
		}
};

class Robot {
public:
	Switches strategy;

	void init() {
		strategy.init();
	}
};

Robot robot;

void setup() {
	Serial.begin(9600);
	robot.init();
}

void loop() {
	robot.strategy.printState();
}
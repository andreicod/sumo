const int CNT_LINE_SENSORS = 2;

class LineSensor {
	int pin, rawVal;
	bool state;
public:
	LineSensor(int _pin = 0) {
		pin = _pin;
		pinMode(pin, INPUT);
	}

	void setPin(int _pin) {
		(*this) = LineSensor(_pin);
	}

	void update() {
		rawVal = analogRead(pin);
		if (rawVal < 200) state = 1;
		else state = 0;
	}

	int getRaw() {
		update();
		return rawVal;
	}

	bool getState() {
		update();
		return state;
	}
};


class LineSensors {
	int n = CNT_LINE_SENSORS;
	int lineSensorsPins[CNT_LINE_SENSORS + 1] = {0, A2, A1};
	LineSensor ls[CNT_LINE_SENSORS + 1];

public:
	int pos = 0;

  	LineSensors() {}

	void setSensor(int poz, int _pin) {
		ls[poz].setPin(_pin);
	}

	void setSensors(int v[]) {
		for (int i = 1; i <= n; ++ i) setSensor(i, v[i]);
	}

	void init() {
		setSensors(lineSensorsPins);
	}

	void updatePosition() {
		pos = 0;
		for (int i = 1; i <= n;  ++ i)
			if (ls[i].getState())
				pos = i;
	}

	int getPosition() {
		updatePosition();
		return pos;
	}

	bool isOnLine() {
		if (getPosition())
			return 1;
		return 0;
	}

	void printPosition() {
		Serial.println(getPosition());
	}

	void printSensorsValues() {
		for (int i = 1; i <= n; ++ i) {
			Serial.print(ls[i].getState());
			Serial.print("		");
		}
		Serial.println();
	}

	void printSensorsRawValues() {
		for (int i = 1; i <= n; ++ i) {
			Serial.print(ls[i].getRaw());
			Serial.print("		");
		}
		Serial.println();
	}

};

class Robot {

public:
	LineSensors line;
	void init() {
		line.init();
	}
};
Robot robot;

void setup() {
	Serial.begin(9600);
  	robot.init();
}

void loop() {
	robot.line.printSensorsValues();
}
